package com.gutho.gutho;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuthoApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuthoApplication.class, args);
    }

}
